# Markdown Experiments

Markdown experiments for LinkedIn Learning.

## Learning Goals

1. Have Fun
2. Learn GitLab
3. ...
4. Profit
5. Spend days relaxing on the beach
6. Run a mile along the Atlantic Coast
7. Learn how to ride a surfboard
     * boogie-board first
     * then the real thing

have a look at https://docs.gitlab.com/ee/user/markdown.html for some features that are supported by GitLab's version of Markdown. 
